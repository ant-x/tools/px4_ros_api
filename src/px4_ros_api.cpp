#include "px4_ros_api/px4_ros_api.hpp"

// Constructor for the Px4RosApi class
Px4RosApi::Px4RosApi() : Node("px4_ros_api")
{
    // Declare and get parameters from the parameter server
    this->declare_parameter<int>("system_id", 1);
    this->get_parameter<int>("system_id", _system_id);
    RCLCPP_INFO(this->get_logger(), "system_id: %d", _system_id);

    this->declare_parameter<int>("buffer_rate", 50);
    this->get_parameter<int>("buffer_rate", _buffer_rate);
    RCLCPP_INFO(this->get_logger(), "buffer_rate: %d", _buffer_rate);

    this->declare_parameter<std::string>("base_link_frame", "base_link");
    this->get_parameter<std::string>("base_link_frame", _base_link_frame);
    RCLCPP_INFO(this->get_logger(), "base_link_frame: %s", _base_link_frame.c_str());

    this->declare_parameter<std::string>("odometry_link_frame", "odom");
    this->get_parameter<std::string>("odometry_link_frame", _odometry_link_frame);
    RCLCPP_INFO(this->get_logger(), "odometry_link_frame: %s", _odometry_link_frame.c_str());

    this->declare_parameter<bool>("publish_map_odom_tf", true);
    this->get_parameter<bool>("publish_map_odom_tf", _publish_map_odom_tf_flag);
    RCLCPP_INFO(this->get_logger(), "publish_map_odom_tf: %s", _publish_map_odom_tf_flag ? "true" : "false");

    this->declare_parameter<std::string>("map_link_frame", "map");
    this->get_parameter<std::string>("map_link_frame", _map_link_frame);
    RCLCPP_INFO(this->get_logger(), "map_link_frame: %s", _map_link_frame.c_str());

    this->declare_parameter<std::vector<double>>("map_to_odom_translation", {0.0, 0.0, 0.0});
    this->get_parameter<std::vector<double>>("map_to_odom_translation", _map_to_odom_translation);
    RCLCPP_INFO(this->get_logger(), "map_to_odom_translation: [%f, %f, %f]", _map_to_odom_translation[0], _map_to_odom_translation[1], _map_to_odom_translation[2]);

    this->declare_parameter<std::vector<double>>("map_to_odom_rpy", {0.0, 0.0, 0.0});
    this->get_parameter<std::vector<double>>("map_to_odom_rpy", _map_to_odom_rpy);
    RCLCPP_INFO(this->get_logger(), "map_to_odom_rpy: [%f, %f, %f]", _map_to_odom_rpy[0], _map_to_odom_rpy[1], _map_to_odom_rpy[2]);

    this->declare_parameter<bool>("subscribe_to_external_odometry", false);
    this->get_parameter<bool>("subscribe_to_external_odometry", _subscribe_to_external_odometry);
    RCLCPP_INFO(this->get_logger(), "subscribe_to_external_odometry: %s", _subscribe_to_external_odometry ? "true" : "false");

    this->declare_parameter<bool>("subscribe_to_external_pose", false);
    this->get_parameter<bool>("subscribe_to_external_pose", _subscribe_to_external_pose);
    RCLCPP_INFO(this->get_logger(), "subscribe_to_external_pose: %s", _subscribe_to_external_pose ? "true" : "false");

    this->declare_parameter<int>("maximum_ext_rate", 100);
    this->get_parameter<int>("maximum_ext_rate", _maximum_ext_rate);
    RCLCPP_INFO(this->get_logger(), "maximum_ext_rate: %d", _maximum_ext_rate);

    if (_subscribe_to_external_odometry && _subscribe_to_external_pose)
    {
        RCLCPP_ERROR(this->get_logger(), "You cannot set both subscribe_to_external_odometry and subscribe_to_external_pose to TRUE, please fix it!");
    }
    else if (_subscribe_to_external_odometry && !_subscribe_to_external_pose)
    {
        _external_odometry_sub =
            this->create_subscription<nav_msgs::msg::Odometry>("externalOdometry/stdMsgs/in", _qos_default,
                                                               std::bind(&Px4RosApi::externalOdometryCallback, this, std::placeholders::_1));
    }
    else if (!_subscribe_to_external_odometry && _subscribe_to_external_pose)
    {
        _external_pose_sub =
            this->create_subscription<geometry_msgs::msg::PoseStamped>("externalPose/stdMsgs/in", _qos_default,
                                                                       std::bind(&Px4RosApi::externalPoseCallback, this, std::placeholders::_1));
    }

    if (_subscribe_to_external_odometry || _subscribe_to_external_pose)
    {
        // Create a timer that triggers the specified callback at a specified frequency
        _timer_visual_odometry =
            this->create_wall_timer(std::chrono::milliseconds(1000 / _maximum_ext_rate),
                                    std::bind(&Px4RosApi::visualOdometryPublishTimerCallback, this));
    }

    // Set QoS
    _qos_latched.transient_local();

    _vehicle_status_sub =
        this->create_subscription<px4_msgs::msg::VehicleStatus>("status/px4Msgs/in", _qos_default,
                                                                std::bind(&Px4RosApi::vehicleStatusCallback, this, std::placeholders::_1));
    _sensor_combined_sub =
        this->create_subscription<px4_msgs::msg::SensorCombined>("imu/px4Msgs/in", _qos_default,
                                                                 std::bind(&Px4RosApi::sensorCombinedCallback, this, std::placeholders::_1));
    _vehicle_odometry_sub =
        this->create_subscription<px4_msgs::msg::VehicleOdometry>("odometry/px4Msgs/in", _qos_default,
                                                                  std::bind(&Px4RosApi::vehicleOdometryCallback, this, std::placeholders::_1));
    _trajectory_setpoint_sub =
        this->create_subscription<px4_ros_extra::msg::PoseTarget>("trajectory/px4RosExtra/in", _qos_default,
                                                                  std::bind(&Px4RosApi::trajectorySetpointCallback, this, std::placeholders::_1));
    _velocity_setpoint_sub =
        this->create_subscription<geometry_msgs::msg::Twist>("velocity/stdMsgs/in", _qos_default,
                                                             std::bind(&Px4RosApi::velocitySetpointCallback, this, std::placeholders::_1));

    // Define publishers for sending messages
    _std_imu_pub =
        this->create_publisher<sensor_msgs::msg::Imu>("imu/stdMsgs/out", _qos_default);
    _std_odometry_pub =
        this->create_publisher<nav_msgs::msg::Odometry>("odometry/stdMsgs/out", _qos_default);
    _std_pose_stamped_pub =
        this->create_publisher<geometry_msgs::msg::PoseStamped>("poseStamped/stdMsgs/out", _qos_default);
    _uav_state_pub =
        this->create_publisher<px4_ros_extra::msg::UavState>("state/px4RosExtra/out", _qos_latched);
    _trajectory_setpoint_pub =
        this->create_publisher<px4_msgs::msg::TrajectorySetpoint>("trajectory/px4Msgs/out", _qos_default);
    _vehicle_command_pub =
        this->create_publisher<px4_msgs::msg::VehicleCommand>("vehicleCommand/px4Msgs/out", _qos_default);
    _offboard_control_mode_pub =
        this->create_publisher<px4_msgs::msg::OffboardControlMode>("controlMode/px4Msgs/out", _qos_default);
    _vehicle_visual_odometry_pub =
        this->create_publisher<px4_msgs::msg::VehicleVisualOdometry>("visualOdometry/px4Msgs/out", _qos_default);

    // Define services for handling requests
    _arm_disarm_server =
        this->create_service<px4_ros_extra::srv::ArmDisarm>("arm_disarm",
                                                            std::bind(&Px4RosApi::handleArmDisarm, this, std::placeholders::_1, std::placeholders::_2));
    _land_server =
        this->create_service<px4_ros_extra::srv::Land>("land",
                                                       std::bind(&Px4RosApi::handleLand, this, std::placeholders::_1, std::placeholders::_2));
    _takeoff_server =
        this->create_service<px4_ros_extra::srv::Takeoff>("takeoff",
                                                          std::bind(&Px4RosApi::handleTakeoff, this, std::placeholders::_1, std::placeholders::_2));

    // Define TF broadcasters
    _map_to_odom_tf_broadcaster = std::make_shared<tf2_ros::StaticTransformBroadcaster>(this);
    _odom_to_base_link_tf_broadcaster = std::make_shared<tf2_ros::TransformBroadcaster>(this);

    // Define TF listeners
    _tf_buffer = std::make_unique<tf2_ros::Buffer>(this->get_clock());
    _tf_listener = std::make_shared<tf2_ros::TransformListener>(*_tf_buffer);

    // Create a timer that triggers the specified callback at a specified frequency
    _timer =
        this->create_wall_timer(std::chrono::milliseconds(1000 / _buffer_rate),
                                std::bind(&Px4RosApi::mainLoop, this));

    // Publish static TF between map and odom frame links
    if (_publish_map_odom_tf_flag)
    {
        publish_map_to_odom_tf();
    }

    // Publish initial state
    _uav_state_msg.state = px4_ros_extra::msg::UavState::DISARMED;
    _uav_state_msg.info = "DISARMED";
    _uav_state_pub->publish(_uav_state_msg);
}

// Main function for the state machine transition handling
void Px4RosApi::updateStateMachine()
{
    if (_uav_state == UavState::DISARMED && _arm_request)
    { // Transition from disarmed to armed
        _arm_request = false;

        _uav_state = UavState::ARMED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::ARMED;
        _uav_state_msg.info = "ARMED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to ARMED");
    }
    else if (_uav_state == UavState::ARMED && (_disarm_request || !_fcu_armed))
    { // Transition from armed to disarmed
        _disarm_request = false;

        _uav_state = UavState::DISARMED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::DISARMED;
        _uav_state_msg.info = "DISARMED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to DISARMED");
    }
    else if (_uav_state == UavState::ARMED && _takeoff_request)
    { // Transition from armed to takeoff
        _takeoff_request = false;

        _uav_state = UavState::TAKEOFF;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::TAKEOFF;
        _uav_state_msg.info = "TAKEOFF";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to TAKEOFF");
    }
    else if (_uav_state == UavState::TAKEOFF && !_fcu_armed)
    { // Transition from takeoff to disarmed
        _uav_state = UavState::DISARMED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::DISARMED;
        _uav_state_msg.info = "DISARMED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to DISARMED");
    }
    else if (_uav_state == UavState::TAKEOFF && !_fcu_offboard)
    { // Transition from takeoff to piloted
        _uav_state = UavState::PILOTED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::PILOTED;
        _uav_state_msg.info = "PILOTED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to PILOTED");
    }
    else if (_uav_state == UavState::TAKEOFF && fabs(_trajectory_setpoint_aux_msg.z - _vehicle_odometry_msg.z) <= 0.2f)
    { // Transition from takeoff to hold
        _uav_state = UavState::HOLD;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::HOLD;
        _uav_state_msg.info = "HOLD";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to HOLD");
    }
    else if (_uav_state == UavState::HOLD && !_fcu_offboard)
    { // Transition from hold to piloted
        _uav_state = UavState::PILOTED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::PILOTED;
        _uav_state_msg.info = "PILOTED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to PILOTED");
    }
    else if (_uav_state == UavState::HOLD && _land_request)
    { // Transition from hold to landing
        _land_request = false;

        _uav_state = UavState::LANDING;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::LANDING;
        _uav_state_msg.info = "LANDING";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to LANDING");
    }
    else if (_uav_state == UavState::INFLIGHT && _land_request)
    { // Transition from inflight to landing
        _land_request = false;

        _uav_state = UavState::LANDING;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::LANDING;
        _uav_state_msg.info = "LANDING";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to LANDING");
    }
    else if (_uav_state == UavState::HOLD && (_received_trajectory_sp || _received_velocity_sp))
    { // Transition from hold to inflight
        _received_velocity_sp = false;
        _received_trajectory_sp = false;

        _uav_state = UavState::INFLIGHT;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::INFLIGHT;
        _uav_state_msg.info = "INFLIGHT";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to INFLIGHT");
    }
    else if (_uav_state == UavState::INFLIGHT && !_fcu_offboard)
    { // Transition from inflight to piloted
        _uav_state = UavState::PILOTED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::PILOTED;
        _uav_state_msg.info = "PILOTED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to PILOTED");
    }
    else if (_uav_state == UavState::LANDING && !_fcu_offboard)
    { // Transition from landing to piloted
        _uav_state = UavState::PILOTED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::PILOTED;
        _uav_state_msg.info = "PILOTED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to PILOTED");
    }
    else if (_uav_state == UavState::LANDING && !_fcu_armed)
    { // Transition from landing to disarmed
        _disarm_request = false;

        _uav_state = UavState::DISARMED;
        _uav_state_msg.state = px4_ros_extra::msg::UavState::DISARMED;
        _uav_state_msg.info = "DISARMED";
        _uav_state_pub->publish(_uav_state_msg);
        RCLCPP_INFO(this->get_logger(), "Transition to DISARMED");
    }
    // else if (_uav_state == UavState::PILOTED && !_fcu_armed)
    // { // Transition from piloted to disarmed
    //     _uav_state = UavState::DISARMED;
    //     _uav_state_msg.state = px4_ros_extra::msg::UavState::DISARMED;
    //     _uav_state_msg.info = "DISARMED";
    //     _uav_state_pub->publish(_uav_state_msg);
    //     RCLCPP_INFO(this->get_logger(), "Transition to DISARMED");
    // }
    else
    {
        // Do nothing
    }
}

// Callback function for receiving vehicle status messages
void Px4RosApi::vehicleStatusCallback(const px4_msgs::msg::VehicleStatus::SharedPtr msg)
{
    _vehicle_status_msg = *msg;

    _fcu_armed = _vehicle_status_msg.arming_state == px4_msgs::msg::VehicleStatus::ARMING_STATE_ARMED;
    _fcu_offboard = _vehicle_status_msg.nav_state == px4_msgs::msg::VehicleStatus::NAVIGATION_STATE_OFFBOARD;

    updateStateMachine();
}

// Callback function for receiving sensor combined messages
void Px4RosApi::sensorCombinedCallback(const px4_msgs::msg::SensorCombined::SharedPtr msg)
{
    // Store the received message
    _sensor_combined_msg = *msg;

    // Set the header timestamp and frame ID
    _output_std_imu_msg.header.stamp = this->now();
    _output_std_imu_msg.header.frame_id = _base_link_frame;

    // Convert orientation from NED to ENU local frame
    Eigen::Quaterniond ned_orientation(
        _vehicle_odometry_msg.q[0], // w
        _vehicle_odometry_msg.q[1], // x
        _vehicle_odometry_msg.q[2], // y
        _vehicle_odometry_msg.q[3]  // z
    );

    // static constexpr double RAD_TO_DEG = (180.0 / M_PI);
    // Eigen::Vector3d ned_euler = ned_orientation.toRotationMatrix().eulerAngles(2, 1, 0).reverse();
    // RCLCPP_INFO(this->get_logger(), "[NED] R: %.2f, P: %.2f, Y: %.2f", ned_euler.x() * RAD_TO_DEG, ned_euler.y() * RAD_TO_DEG, ned_euler.z() * RAD_TO_DEG);
    Eigen::Quaterniond enu_orientation = aircraft_to_baselink_orientation(ned_to_enu_orientation(ned_orientation));

    // Populate the orientation fields
    _output_std_imu_msg.orientation.x = enu_orientation.x();
    _output_std_imu_msg.orientation.y = enu_orientation.y();
    _output_std_imu_msg.orientation.z = enu_orientation.z();
    _output_std_imu_msg.orientation.w = enu_orientation.w();

    // static constexpr double RAD_TO_DEG = (180.0 / M_PI);
    // Eigen::Vector3d enu_euler = enu_orientation.toRotationMatrix().eulerAngles(2, 1, 0);
    // RCLCPP_INFO(this->get_logger(), "[ENU] R: %.2f, P: %.2f, Y: %.2f", enu_euler.x() * RAD_TO_DEG, enu_euler.y() * RAD_TO_DEG, enu_euler.z() * RAD_TO_DEG);

    // Convert angular velocity from FRD to FLU body frame
    Eigen::Vector3d frd_angular_velocity(
        _sensor_combined_msg.gyro_rad[0],
        _sensor_combined_msg.gyro_rad[1],
        _sensor_combined_msg.gyro_rad[2]);
    Eigen::Vector3d flu_angular_velocity = aircraft_to_baselink_body_frame(frd_angular_velocity);
    _output_std_imu_msg.angular_velocity.x = flu_angular_velocity.x();
    _output_std_imu_msg.angular_velocity.y = flu_angular_velocity.y();
    _output_std_imu_msg.angular_velocity.z = flu_angular_velocity.z();

    // Convert accelerometer data from FRD to FLU body frame
    Eigen::Vector3d frd_acceleration(
        _sensor_combined_msg.accelerometer_m_s2[0],
        _sensor_combined_msg.accelerometer_m_s2[1],
        _sensor_combined_msg.accelerometer_m_s2[2]);
    Eigen::Vector3d flu_acceleration = aircraft_to_baselink_body_frame(frd_acceleration);
    _output_std_imu_msg.linear_acceleration.x = flu_acceleration.x();
    _output_std_imu_msg.linear_acceleration.y = flu_acceleration.y();
    _output_std_imu_msg.linear_acceleration.z = flu_acceleration.z();

    // Publish the standard IMU message
    _std_imu_pub->publish(_output_std_imu_msg);
}

// Callback function for receiving vehicle odometry messages
void Px4RosApi::vehicleOdometryCallback(const px4_msgs::msg::VehicleOdometry::SharedPtr msg)
{
    // Store the received message
    _vehicle_odometry_msg = *msg;

    // Set the header timestamp, frame ID, and child frame ID
    _output_std_odom_msg.header.stamp = this->now();
    _output_std_odom_msg.header.frame_id = _odometry_link_frame;
    _output_std_odom_msg.child_frame_id = _base_link_frame;

    // Convert position based on local frame
    if (_vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_NED || // NED earth-fixed frame
        _vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_FRD)   // FRD earth-fixed frame, arbitrary heading reference
    {
        // Convert from NED to ENU body frame
        Eigen::Vector3d ned_position(
            _vehicle_odometry_msg.x - _odom_offset_north,
            _vehicle_odometry_msg.y - _odom_offset_east,
            _vehicle_odometry_msg.z - _odom_offset_down);
        Eigen::Vector3d enu_position = ned_to_enu_local_frame(ned_position);
        _output_std_odom_msg.pose.pose.position.x = enu_position.x();
        _output_std_odom_msg.pose.pose.position.y = enu_position.y();
        _output_std_odom_msg.pose.pose.position.z = enu_position.z();
    }
    else if (_vehicle_odometry_msg.local_frame == px4_msgs::msg::VehicleOdometry::BODY_FRAME_FRD) // FRD body-fixed frame
    {
        // Convert from FRD to FLU body frame
        Eigen::Vector3d frd_position(
            _vehicle_odometry_msg.x - _odom_offset_north,
            _vehicle_odometry_msg.y - _odom_offset_east,
            _vehicle_odometry_msg.z - _odom_offset_down);
        Eigen::Vector3d flu_position = aircraft_to_baselink_body_frame(frd_position);
        _output_std_odom_msg.pose.pose.position.x = flu_position.x();
        _output_std_odom_msg.pose.pose.position.y = flu_position.y();
        _output_std_odom_msg.pose.pose.position.z = flu_position.z();
    }
    else
    {
        // Not aligned with the standard frames of reference -> do not rotate
        _output_std_odom_msg.pose.pose.position.x = _vehicle_odometry_msg.x - _odom_offset_north;
        _output_std_odom_msg.pose.pose.position.y = _vehicle_odometry_msg.y - _odom_offset_east;
        _output_std_odom_msg.pose.pose.position.z = _vehicle_odometry_msg.z - _odom_offset_down;
    }

    // Convert Quaternion orientation from NED to ENU local frame
    Eigen::Quaterniond ned_orientation(
        _vehicle_odometry_msg.q[0], // w
        _vehicle_odometry_msg.q[1], // x
        _vehicle_odometry_msg.q[2], // y
        _vehicle_odometry_msg.q[3]  // z
    );
    Eigen::Quaterniond enu_orientation = aircraft_to_baselink_orientation(ned_to_enu_orientation(ned_orientation));
    _output_std_odom_msg.pose.pose.orientation.x = enu_orientation.x();
    _output_std_odom_msg.pose.pose.orientation.y = enu_orientation.y();
    _output_std_odom_msg.pose.pose.orientation.z = enu_orientation.z();
    _output_std_odom_msg.pose.pose.orientation.w = enu_orientation.w();

    // Build 6x6 pose covariance matrix
    Matrix6d cov_pose = Matrix6d::Zero();
    array_urt_to_covariance_matrix_custom(_vehicle_odometry_msg.pose_covariance, cov_pose);
    Eigen::Map<Matrix6d>(_output_std_odom_msg.pose.covariance.data(), cov_pose.rows(), cov_pose.cols()) = cov_pose;

    // Convert linear velocity
    if (_vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_NED || // NED earth-fixed frame
        _vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::LOCAL_FRAME_FRD)   // FRD earth-fixed frame, arbitrary heading reference
    {
        // Convert from NED to ENU body frame
        Eigen::Vector3d ned_linear_velocity(
            _vehicle_odometry_msg.vx,
            _vehicle_odometry_msg.vy,
            _vehicle_odometry_msg.vz);
        Eigen::Vector3d enu_linear_velocity = ned_to_enu_local_frame(ned_linear_velocity);
        _output_std_odom_msg.twist.twist.linear.x = enu_linear_velocity.x();
        _output_std_odom_msg.twist.twist.linear.y = enu_linear_velocity.y();
        _output_std_odom_msg.twist.twist.linear.z = enu_linear_velocity.z();
    }
    else if (_vehicle_odometry_msg.velocity_frame == px4_msgs::msg::VehicleOdometry::BODY_FRAME_FRD) // FRD body-fixed frame
    {
        // Convert from FRD to FLU body frame
        Eigen::Vector3d frd_linear_velocity(
            _vehicle_odometry_msg.vx,
            _vehicle_odometry_msg.vy,
            _vehicle_odometry_msg.vz);
        Eigen::Vector3d flu_linear_velocity = aircraft_to_baselink_body_frame(frd_linear_velocity);
        _output_std_odom_msg.twist.twist.linear.x = flu_linear_velocity.x();
        _output_std_odom_msg.twist.twist.linear.y = flu_linear_velocity.y();
        _output_std_odom_msg.twist.twist.linear.z = flu_linear_velocity.z();
    }
    else
    {
        // Not aligned with the standard frames of reference -> do not rotate
        _output_std_odom_msg.twist.twist.linear.x = _vehicle_odometry_msg.vx;
        _output_std_odom_msg.twist.twist.linear.y = _vehicle_odometry_msg.vy;
        _output_std_odom_msg.twist.twist.linear.z = _vehicle_odometry_msg.vz;
    }

    // Convert angular velocity from FRD to FLU body frame
    Eigen::Vector3d frd_angular_velocity(
        _vehicle_odometry_msg.rollspeed,
        _vehicle_odometry_msg.pitchspeed,
        _vehicle_odometry_msg.yawspeed);
    Eigen::Vector3d flu_angular_velocity = aircraft_to_baselink_body_frame(frd_angular_velocity);
    _output_std_odom_msg.twist.twist.angular.x = flu_angular_velocity.x();
    _output_std_odom_msg.twist.twist.angular.y = flu_angular_velocity.y();
    _output_std_odom_msg.twist.twist.angular.z = flu_angular_velocity.z();

    // Build 6x6 velocity covariance matrix
    Matrix6d cov_vel = Matrix6d::Zero();
    array_urt_to_covariance_matrix_custom(_vehicle_odometry_msg.velocity_covariance, cov_vel);
    Eigen::Map<Matrix6d>(_output_std_odom_msg.twist.covariance.data(), cov_vel.rows(), cov_vel.cols()) = cov_vel;

    // Publish the standard odometry message
    _std_odometry_pub->publish(_output_std_odom_msg);

    // Publish dynamic TF between odometry frame and base_link frame
    publish_odom_to_base_link_tf(_output_std_odom_msg);

    // Transform the odometry into map pose and publish it
    _output_std_pose_stamped = transform_odometry_to_map_pose(_output_std_odom_msg);
    _output_std_pose_stamped.header.stamp = now();
    _std_pose_stamped_pub->publish(_output_std_pose_stamped);
}

// Callback function for receiving trajectory setpoint messages
void Px4RosApi::trajectorySetpointCallback(const px4_ros_extra::msg::PoseTarget::SharedPtr msg)
{
    _received_trajectory_sp = true;
    updateStateMachine();

    // Store the received message and convert the setpoint from map to odom frame
    _trajectory_setpoint_input_msg = transform_trajectory_from_map_to_odom(*msg);

    // Convert position from ENU to NED local frame
    Eigen::Vector3d enu_position(
        _trajectory_setpoint_input_msg.position.x,
        _trajectory_setpoint_input_msg.position.y,
        _trajectory_setpoint_input_msg.position.z);
    Eigen::Vector3d ned_position = enu_to_ned_local_frame(enu_position);
    // Store converted position
    _trajectory_setpoint_output_msg.x = _odom_offset_north + ned_position.x();
    _trajectory_setpoint_output_msg.y = _odom_offset_east + ned_position.y();
    _trajectory_setpoint_output_msg.z = _odom_offset_down + ned_position.z();

    // Convert orientation from ENU->FLU to NED->FRD local frame
    Eigen::Quaterniond enu_orientation(
        _trajectory_setpoint_input_msg.orientation.w, // w
        _trajectory_setpoint_input_msg.orientation.x, // x
        _trajectory_setpoint_input_msg.orientation.y, // y
        _trajectory_setpoint_input_msg.orientation.z  // z
    );
    // Convert and store orientation
    Eigen::Quaterniond ned_orientation = enu_to_ned_orientation(baselink_to_aircraft_orientation(enu_orientation));
    _trajectory_setpoint_output_msg.yaw = quaternion_get_yaw(ned_orientation);

    // Convert angular velocity from FLU to FRD body frame
    Eigen::Vector3d flu_angular_velocity(
        _trajectory_setpoint_input_msg.body_rate.x,
        _trajectory_setpoint_input_msg.body_rate.y,
        _trajectory_setpoint_input_msg.body_rate.z);
    Eigen::Vector3d frd_angular_velocity = baselink_to_aircraft_body_frame(flu_angular_velocity);
    // Store converted angular velocity
    _trajectory_setpoint_output_msg.yawspeed = frd_angular_velocity.z();

    // Convert linear velocity from ENU to NED local frame
    Eigen::Vector3d enu_linear_velocity(
        _trajectory_setpoint_input_msg.velocity.x,
        _trajectory_setpoint_input_msg.velocity.y,
        _trajectory_setpoint_input_msg.velocity.z);
    Eigen::Vector3d ned_linear_velocity = enu_to_ned_local_frame(enu_linear_velocity);
    // Store converted linear velocity
    _trajectory_setpoint_output_msg.vx = ned_linear_velocity.x();
    _trajectory_setpoint_output_msg.vy = ned_linear_velocity.y();
    _trajectory_setpoint_output_msg.vz = ned_linear_velocity.z();
}

// Callback function for receiving velocity setpoint messages
void Px4RosApi::velocitySetpointCallback(const geometry_msgs::msg::Twist::SharedPtr msg)
{
    _received_velocity_sp = true;
    updateStateMachine();

    // Store the received message
    _velocity_setpoint_input_msg = *msg;

    // Convert angular velocity from FLU to FRD body frame
    Eigen::Vector3d flu_angular_velocity(
        _velocity_setpoint_input_msg.angular.x,
        _velocity_setpoint_input_msg.angular.y,
        _velocity_setpoint_input_msg.angular.z);
    Eigen::Vector3d frd_angular_velocity = baselink_to_aircraft_body_frame(flu_angular_velocity);

    // Store converted angular velocity
    _trajectory_setpoint_output_msg.yawspeed = frd_angular_velocity.z();

    // Convert linear velocity from FLU to FRD body frame
    Eigen::Vector3d flu_linear_velocity(
        _velocity_setpoint_input_msg.linear.x,
        _velocity_setpoint_input_msg.linear.y,
        _velocity_setpoint_input_msg.linear.z);
    Eigen::Vector3d frd_linear_velocity = baselink_to_aircraft_body_frame(flu_linear_velocity);

    // Convert FRD body frame to NED local frame
    Eigen::Quaterniond ned_orientation(
        _vehicle_odometry_msg.q[0], // w
        _vehicle_odometry_msg.q[1], // x
        _vehicle_odometry_msg.q[2], // y
        _vehicle_odometry_msg.q[3]  // z
    );
    Eigen::Vector3d ned_linear_velocity = aircraft_to_ned_frame(frd_linear_velocity, ned_orientation);

    // Store converted linear velocity
    _trajectory_setpoint_output_msg.vx = ned_linear_velocity.x();
    _trajectory_setpoint_output_msg.vy = ned_linear_velocity.y();
    _trajectory_setpoint_output_msg.vz = ned_linear_velocity.z();

    // Set to NaN all the uncontrolled states
    _trajectory_setpoint_output_msg.x = NAN;
    _trajectory_setpoint_output_msg.y = NAN;
    _trajectory_setpoint_output_msg.z = NAN;
    _trajectory_setpoint_output_msg.yaw = NAN;
}

// Callback function for receiving external odometry messages
void Px4RosApi::externalOdometryCallback(const nav_msgs::msg::Odometry::SharedPtr msg)
{
    // Store the received message
    _external_odometry_msg = *msg;

    builtin_interfaces::msg::Time timestamp = _external_odometry_msg.header.stamp;
    _vehicle_visual_odometry_msg.timestamp_sample = (timestamp.sec * 1e9 + timestamp.nanosec) / 1000;

    _vehicle_visual_odometry_msg.local_frame = px4_msgs::msg::VehicleVisualOdometry::LOCAL_FRAME_NED;

    // Convert from ENU to NED body frame
    Eigen::Vector3d enu_position(
        _external_odometry_msg.pose.pose.position.x,
        _external_odometry_msg.pose.pose.position.y,
        _external_odometry_msg.pose.pose.position.z);

    Eigen::Vector3d ned_position = enu_to_ned_local_frame(enu_position);

    _vehicle_visual_odometry_msg.x = ned_position.x();
    _vehicle_visual_odometry_msg.y = ned_position.y();
    _vehicle_visual_odometry_msg.z = ned_position.z();

    // Convert Quaternion orientation from ENU to NED local frame
    Eigen::Quaterniond enu_orientation(
        _external_odometry_msg.pose.pose.orientation.w,
        _external_odometry_msg.pose.pose.orientation.x,
        _external_odometry_msg.pose.pose.orientation.y,
        _external_odometry_msg.pose.pose.orientation.z);

    Eigen::Quaterniond ned_orientation = enu_to_ned_orientation(baselink_to_aircraft_orientation(enu_orientation));

    _vehicle_visual_odometry_msg.q[0] = ned_orientation.w();
    _vehicle_visual_odometry_msg.q[1] = ned_orientation.x();
    _vehicle_visual_odometry_msg.q[2] = ned_orientation.y();
    _vehicle_visual_odometry_msg.q[3] = ned_orientation.z();

    _vehicle_visual_odometry_msg.q_offset[0] = 1.0f;
    _vehicle_visual_odometry_msg.q_offset[1] = 0.0f;
    _vehicle_visual_odometry_msg.q_offset[2] = 0.0f;
    _vehicle_visual_odometry_msg.q_offset[3] = 0.0f;

    // Build row-major representation of 6x6 pose cross-covariance matrix URT
    Matrix6d covariance_matrix;
    covariance_matrix.setZero();
    for (size_t i = 0; i < 36; ++i)
    {
        size_t row = i / 6; // Compute row index
        size_t col = i % 6; // Compute column index
        covariance_matrix(row, col) = static_cast<double>(_external_odometry_msg.pose.covariance[i]);
    }
    covariance_matrix_to_array_urt_custom(covariance_matrix, _vehicle_visual_odometry_msg.pose_covariance);

    _vehicle_visual_odometry_msg.velocity_frame = px4_msgs::msg::VehicleVisualOdometry::LOCAL_FRAME_FRD;

    // Convert from FLU to FRD body frame
    Eigen::Vector3d flu_linear_velocity(
        _external_odometry_msg.twist.twist.linear.x,
        _external_odometry_msg.twist.twist.linear.y,
        _external_odometry_msg.twist.twist.linear.z);

    Eigen::Vector3d frd_linear_velocity = baselink_to_aircraft_body_frame(flu_linear_velocity);

    _vehicle_visual_odometry_msg.vx = frd_linear_velocity.x();
    _vehicle_visual_odometry_msg.vy = frd_linear_velocity.y();
    _vehicle_visual_odometry_msg.vz = frd_linear_velocity.z();

    // Convert angular velocity from FLU to FRD body frame
    Eigen::Vector3d flu_angular_velocity(
        _external_odometry_msg.twist.twist.angular.x,
        _external_odometry_msg.twist.twist.angular.y,
        _external_odometry_msg.twist.twist.angular.z);

    Eigen::Vector3d frd_angular_velocity = baselink_to_aircraft_body_frame(flu_angular_velocity);

    _vehicle_visual_odometry_msg.rollspeed = frd_angular_velocity.x();
    _vehicle_visual_odometry_msg.pitchspeed = frd_angular_velocity.y();
    _vehicle_visual_odometry_msg.yawspeed = frd_angular_velocity.z();

    // Build row-major representation of 6x6 velocity cross-covariance matrix URT
    covariance_matrix.setZero();
    for (size_t i = 0; i < 36; ++i)
    {
        size_t row = i / 6; // Compute row index
        size_t col = i % 6; // Compute column index
        covariance_matrix(row, col) = static_cast<double>(_external_odometry_msg.twist.covariance[i]);
    }
    covariance_matrix_to_array_urt_custom(covariance_matrix, _vehicle_visual_odometry_msg.velocity_covariance);

    // Create timestamp
    _vehicle_visual_odometry_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
}

// Callback function for receiving external pose messages
void Px4RosApi::externalPoseCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
{
    // Store the received message
    _external_pose_msg = *msg;

    builtin_interfaces::msg::Time timestamp = _external_pose_msg.header.stamp;
    _vehicle_visual_odometry_msg.timestamp_sample = (timestamp.sec * 1e9 + timestamp.nanosec) / 1000;

    _vehicle_visual_odometry_msg.local_frame = px4_msgs::msg::VehicleVisualOdometry::LOCAL_FRAME_NED;

    // Convert from ENU to NED body frame
    Eigen::Vector3d enu_position(
        _external_pose_msg.pose.position.x,
        _external_pose_msg.pose.position.y,
        _external_pose_msg.pose.position.z);

    Eigen::Vector3d ned_position = enu_to_ned_local_frame(enu_position);

    _vehicle_visual_odometry_msg.x = ned_position.x();
    _vehicle_visual_odometry_msg.y = ned_position.y();
    _vehicle_visual_odometry_msg.z = ned_position.z();

    // Convert Quaternion orientation from ENU to NED local frame
    Eigen::Quaterniond enu_orientation(
        _external_pose_msg.pose.orientation.w,
        _external_pose_msg.pose.orientation.x,
        _external_pose_msg.pose.orientation.y,
        _external_pose_msg.pose.orientation.z);

    Eigen::Quaterniond ned_orientation = enu_to_ned_orientation(baselink_to_aircraft_orientation(enu_orientation));

    _vehicle_visual_odometry_msg.q[0] = ned_orientation.w();
    _vehicle_visual_odometry_msg.q[1] = ned_orientation.x();
    _vehicle_visual_odometry_msg.q[2] = ned_orientation.y();
    _vehicle_visual_odometry_msg.q[3] = ned_orientation.z();

    _vehicle_visual_odometry_msg.q_offset[0] = 1.0f;
    _vehicle_visual_odometry_msg.q_offset[1] = 0.0f;
    _vehicle_visual_odometry_msg.q_offset[2] = 0.0f;
    _vehicle_visual_odometry_msg.q_offset[3] = 0.0f;

    // Build row-major representation of 6x6 pose cross-covariance matrix URT with an identity matrix
    _vehicle_visual_odometry_msg.pose_covariance[0] = 0.0f;  // Variance of position along x
    _vehicle_visual_odometry_msg.pose_covariance[6] = 0.0f;  // Variance of position along y
    _vehicle_visual_odometry_msg.pose_covariance[11] = 0.0f; // Variance of position along z
    _vehicle_visual_odometry_msg.pose_covariance[15] = 0.0f; // Variance of rotation about the x axis
    _vehicle_visual_odometry_msg.pose_covariance[20] = 0.0f; // Variance of rotation about the y axis
    _vehicle_visual_odometry_msg.pose_covariance[25] = 0.0f; // Variance of rotation about the z axis

    _vehicle_visual_odometry_msg.velocity_frame = px4_msgs::msg::VehicleVisualOdometry::LOCAL_FRAME_FRD;

    _vehicle_visual_odometry_msg.vx = NAN;
    _vehicle_visual_odometry_msg.vy = NAN;
    _vehicle_visual_odometry_msg.vz = NAN;

    _vehicle_visual_odometry_msg.rollspeed = NAN;
    _vehicle_visual_odometry_msg.pitchspeed = NAN;
    _vehicle_visual_odometry_msg.yawspeed = NAN;

    // Build row-major representation of 6x6 velocity cross-covariance matrix URT with an identity matrix
    _vehicle_visual_odometry_msg.velocity_covariance[0] = 0.0f;  // Variance of position along x
    _vehicle_visual_odometry_msg.velocity_covariance[6] = 0.0f;  // Variance of position along y
    _vehicle_visual_odometry_msg.velocity_covariance[11] = 0.0f; // Variance of position along z
    _vehicle_visual_odometry_msg.velocity_covariance[15] = 0.0f; // Variance of rotation about the x axis
    _vehicle_visual_odometry_msg.velocity_covariance[20] = 0.0f; // Variance of rotation about the y axis
    _vehicle_visual_odometry_msg.velocity_covariance[25] = 0.0f; // Variance of rotation about the z axis

    // Create timestamp
    _vehicle_visual_odometry_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
}

// Callback function for publishing external pose messages
void Px4RosApi::visualOdometryPublishTimerCallback()
{
    // Send vehicle_visual_odometry message if old timestamp is != new timestamp
    if (_old_vehicle_visual_odometry_msg_timestamp != _vehicle_visual_odometry_msg.timestamp)
    {
        _vehicle_visual_odometry_pub->publish(_vehicle_visual_odometry_msg);
    }
    _old_vehicle_visual_odometry_msg_timestamp = _vehicle_visual_odometry_msg.timestamp;
}

// Callback function for handling trajectory setpoint updates
void Px4RosApi::mainLoop()
{
    // Check if the UAV state
    if (_uav_state == UavState::DISARMED)
    {
        _received_trajectory_sp = false;
        _received_velocity_sp = false;
    }
    else if (_uav_state == UavState::ARMED || _uav_state == UavState::TAKEOFF || _uav_state == UavState::LANDING || _uav_state == UavState::HOLD)
    {
        // Publish _offboard_control_mode_msg
        _offboard_control_mode_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
        _offboard_control_mode_pub->publish(_offboard_control_mode_msg);

        // Use auxiliary trajectory message
        _trajectory_setpoint_aux_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
        _trajectory_setpoint_pub->publish(_trajectory_setpoint_aux_msg);
    }
    else if (_uav_state == UavState::INFLIGHT)
    {
        // Publish _offboard_control_mode_msg
        _offboard_control_mode_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
        _offboard_control_mode_pub->publish(_offboard_control_mode_msg);

        // If in flight, publish trajectory setpoint message
        _trajectory_setpoint_output_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
        _trajectory_setpoint_pub->publish(_trajectory_setpoint_output_msg);
    }
    else
    {
        // Do nothing
    }
}

// Service callback function for arming and disarming the UAV
void Px4RosApi::handleArmDisarm(
    const std::shared_ptr<px4_ros_extra::srv::ArmDisarm::Request> request,
    const std::shared_ptr<px4_ros_extra::srv::ArmDisarm::Response> response)
{
    if (request->arm)
    {
        RCLCPP_INFO(this->get_logger(), "Arm request received");
        if (_uav_state == UavState::DISARMED)
        {
            _arm_request = true;
            updateStateMachine();

            // Arm the vehicle
            sendVehicleCommand(px4_msgs::msg::VehicleCommand::VEHICLE_CMD_COMPONENT_ARM_DISARM, 1.0);
            RCLCPP_INFO(this->get_logger(), "Arm request sent to FCU");

            // Set offboard mode
            _offboard_control_mode_msg.position = true;
            _offboard_control_mode_msg.velocity = true;
            _offboard_control_mode_msg.acceleration = true;
            _offboard_control_mode_msg.attitude = true;
            _offboard_control_mode_msg.body_rate = true;
            _offboard_control_mode_msg.actuator = false;

            // Set armed setpoint
            _trajectory_setpoint_aux_msg.x = NAN;         // North
            _trajectory_setpoint_aux_msg.y = NAN;         // East
            _trajectory_setpoint_aux_msg.z = NAN;         // Down
            _trajectory_setpoint_aux_msg.vx = 0.0f;       // North speed
            _trajectory_setpoint_aux_msg.vy = 0.0f;       // East speed
            _trajectory_setpoint_aux_msg.vz = 1.0f;       // Down speed
            _trajectory_setpoint_aux_msg.yaw = NAN;       // Yaw wrt NED
            _trajectory_setpoint_aux_msg.yawspeed = 0.0f; // Yaw speed wrt NED

            response->success = true;
        }
        else
        {
            RCLCPP_INFO(this->get_logger(), "Already armed");
            response->success = false;
        }
    }
    else
    {
        RCLCPP_INFO(this->get_logger(), "Disarming request received");
        if (_uav_state == UavState::ARMED || _uav_state == UavState::LANDING)
        {
            _disarm_request = true;
            updateStateMachine();

            // Disarm the vehicle
            sendVehicleCommand(px4_msgs::msg::VehicleCommand::VEHICLE_CMD_COMPONENT_ARM_DISARM, 0.0);
            RCLCPP_INFO(this->get_logger(), "Disarm request sent to FCU");

            response->success = true;
        }
        else if (_uav_state == UavState::DISARMED)
        {
            RCLCPP_INFO(this->get_logger(), "Already disarmed");

            response->success = false;
        }
        else
        {
            RCLCPP_INFO(this->get_logger(), "Can not disarm");

            response->success = false;
        }
    }
}

// Service callback function for handling landing requests
void Px4RosApi::handleLand(
    const std::shared_ptr<px4_ros_extra::srv::Land::Request> request,
    const std::shared_ptr<px4_ros_extra::srv::Land::Response> response)
{
    // Landing request received
    RCLCPP_INFO(this->get_logger(), "Landing request received");
    if (_uav_state == UavState::HOLD || _uav_state == UavState::INFLIGHT)
    {
        _land_request = true;
        updateStateMachine();

        RCLCPP_INFO(this->get_logger(), "Landing request accepted");
        float landing_speed = std::max(request->landing_speed, 0.0f);
        // we do not need to convert enu to ned as the takeoff altitude since "landing speed" implicitly indicates a positive downward speed (as ned).

        // Set landing setpoint
        _trajectory_setpoint_aux_msg.x = _vehicle_odometry_msg.x; // North
        _trajectory_setpoint_aux_msg.y = _vehicle_odometry_msg.y; // East
        _trajectory_setpoint_aux_msg.z = NAN;                     // Down
        _trajectory_setpoint_aux_msg.vx = 0.0f;                   // North speed
        _trajectory_setpoint_aux_msg.vy = 0.0f;                   // East speed
        _trajectory_setpoint_aux_msg.vz = landing_speed;          // Down speed

        Eigen::Quaterniond ned_orientation(
            _vehicle_odometry_msg.q[0], // w
            _vehicle_odometry_msg.q[1], // x
            _vehicle_odometry_msg.q[2], // y
            _vehicle_odometry_msg.q[3]  // z
        );
        _trajectory_setpoint_aux_msg.yaw = quaternion_get_yaw(ned_orientation); // Yaw wrt NED
        _trajectory_setpoint_aux_msg.yawspeed = 0.0f;                           // Yaw speed wrt NED

        response->success = true;
    }
    else
    {
        RCLCPP_INFO(this->get_logger(), "Landing request rejected");
        response->success = false;
    }
}

// Service callback function for handling takeoff requests
void Px4RosApi::handleTakeoff(
    const std::shared_ptr<px4_ros_extra::srv::Takeoff::Request> request,
    const std::shared_ptr<px4_ros_extra::srv::Takeoff::Response> response)
{

    // Takeoff request received
    RCLCPP_INFO(this->get_logger(), "Takeoff request received");
    if (_uav_state == UavState::ARMED)
    {
        _takeoff_request = true;
        updateStateMachine();

        RCLCPP_INFO(this->get_logger(), "Takeoff request accepted");
        float takeoff_altitude_enu = std::max(request->takeoff_altitude, 0.0f);
        float takeoff_altitude_ned = -takeoff_altitude_enu;

        _odom_offset_north = _vehicle_odometry_msg.x;
        _odom_offset_east = _vehicle_odometry_msg.y;
        _odom_offset_down = _vehicle_odometry_msg.z;

        // Set offboard mode
        sendVehicleCommand(px4_msgs::msg::VehicleCommand::VEHICLE_CMD_DO_SET_MODE, 1, 6);

        // Set takeoff setpoint
        _trajectory_setpoint_aux_msg.x = _odom_offset_north;                       // North
        _trajectory_setpoint_aux_msg.y = _odom_offset_east;                        // East
        _trajectory_setpoint_aux_msg.z = _odom_offset_down + takeoff_altitude_ned; // Down
        _trajectory_setpoint_aux_msg.vx = 0.0f;                                    // North speed
        _trajectory_setpoint_aux_msg.vy = 0.0f;                                    // East speed
        _trajectory_setpoint_aux_msg.vz = 0.0f;                                    // Down Speed

        Eigen::Quaterniond ned_orientation(
            _vehicle_odometry_msg.q[0], // w
            _vehicle_odometry_msg.q[1], // x
            _vehicle_odometry_msg.q[2], // y
            _vehicle_odometry_msg.q[3]  // z
        );
        _trajectory_setpoint_aux_msg.yaw = quaternion_get_yaw(ned_orientation); // Yaw wrt NED
        _trajectory_setpoint_aux_msg.yawspeed = 0.0f;                           // Yaw speed wrt NED

        response->success = true;
    }
    else
    {
        RCLCPP_INFO(this->get_logger(), "Takeoff request rejected");
        response->success = false;
    }
}

// Utility function to send vehicle command messages
void Px4RosApi::sendVehicleCommand(uint16_t command, float param1, float param2)
{
    _vehicle_command_msg.timestamp = this->get_clock()->now().nanoseconds() / 1000;
    _vehicle_command_msg.param1 = param1;
    _vehicle_command_msg.param2 = param2;
    _vehicle_command_msg.command = command;
    _vehicle_command_msg.target_system = _system_id;
    _vehicle_command_msg.target_component = MAV_COMP_ID_AUTOPILOT1;
    _vehicle_command_msg.source_system = _system_id;
    _vehicle_command_msg.source_component = MAV_COMP_ID_AUTOPILOT1;
    _vehicle_command_msg.from_external = true;

    _vehicle_command_pub->publish(_vehicle_command_msg);
}

// Function to send static TF between map and odom frame links
void Px4RosApi::publish_map_to_odom_tf()
{
    geometry_msgs::msg::TransformStamped static_transformStamped;

    static_transformStamped.header.stamp = now();
    static_transformStamped.header.frame_id = _map_link_frame;
    static_transformStamped.child_frame_id = _odometry_link_frame;

    static_transformStamped.transform.translation.x = _map_to_odom_translation[0];
    static_transformStamped.transform.translation.y = _map_to_odom_translation[1];
    static_transformStamped.transform.translation.z = _map_to_odom_translation[2];

    tf2::Quaternion quat;
    quat.setRPY(_map_to_odom_rpy[0], _map_to_odom_rpy[1], _map_to_odom_rpy[2]);
    static_transformStamped.transform.rotation.x = quat.x();
    static_transformStamped.transform.rotation.y = quat.y();
    static_transformStamped.transform.rotation.z = quat.z();
    static_transformStamped.transform.rotation.w = quat.w();

    _map_to_odom_tf_broadcaster->sendTransform(static_transformStamped);
}

// Function to send dynamic TF between odom and base_link frame links
void Px4RosApi::publish_odom_to_base_link_tf(const nav_msgs::msg::Odometry &msg)
{
    geometry_msgs::msg::TransformStamped dynamic_transformStamped;

    dynamic_transformStamped.header.stamp = now();
    dynamic_transformStamped.header.frame_id = _odometry_link_frame;
    dynamic_transformStamped.child_frame_id = _base_link_frame;

    dynamic_transformStamped.transform.translation.x = msg.pose.pose.position.x;
    dynamic_transformStamped.transform.translation.y = msg.pose.pose.position.y;
    dynamic_transformStamped.transform.translation.z = msg.pose.pose.position.z;

    dynamic_transformStamped.transform.rotation = msg.pose.pose.orientation;

    _odom_to_base_link_tf_broadcaster->sendTransform(dynamic_transformStamped);
}

// Utility function to convert an array representation of a upper right triangular matrix to a covariance matrix
void Px4RosApi::array_urt_to_covariance_matrix_custom(const std::array<float, 21> &covmsg, Matrix6d &covmat)
{
    auto in = covmsg.begin();

    // Iterate over the upper right triangular matrix elements
    for (long int x = 0; x < covmat.cols(); x++)
    {
        for (long int y = x; y < covmat.rows(); y++)
        {
            // Fill the covariance matrix symmetrically
            covmat(x, y) = static_cast<double>(*in++);
            covmat(y, x) = covmat(x, y);
        }
    }
}

// Utility function to convert a covariance matrix to an array representation of upper right triangular matrix
void Px4RosApi::covariance_matrix_to_array_urt_custom(const Matrix6d &covmat, std::array<float, 21> &covmsg)
{
    auto out = covmsg.begin();

    // Iterate over the upper right triangular matrix elements
    for (long int x = 0; x < covmat.cols(); x++)
    {
        for (long int y = x; y < covmat.rows(); y++)
        {
            // Extract the upper right triangular matrix elements and store them in the array
            *out++ = static_cast<float>(covmat(x, y));
        }
    }
}

// Utility function to transform odometry message to map pose
geometry_msgs::msg::PoseStamped Px4RosApi::transform_odometry_to_map_pose(const nav_msgs::msg::Odometry &odom_msg)
{
    geometry_msgs::msg::PoseStamped pose_msg;

    try
    {
        // Lookup the "odom" -> "map" transform at the current time
        geometry_msgs::msg::TransformStamped odom_to_map_transform;
        odom_to_map_transform = _tf_buffer->lookupTransform(_map_link_frame, _odometry_link_frame, tf2::TimePointZero);

        // Convert Odometry message to PoseStamped
        geometry_msgs::msg::PoseStamped odom_pose_stamped;
        odom_pose_stamped.header = odom_msg.header;
        odom_pose_stamped.pose = odom_msg.pose.pose;

        // Transform the Odometry pose into the "map" frame
        tf2::doTransform(odom_pose_stamped, pose_msg, odom_to_map_transform);
    }
    catch (tf2::TransformException &ex)
    {
        // Handle exceptions in case the transform is not available
        RCLCPP_INFO(this->get_logger(), "Could not transform %s to %s: %s", _odometry_link_frame.c_str(), _map_link_frame.c_str(), ex.what());
    }

    return pose_msg;
}

// Utility function to transform trajectory message from map to odometry frame
px4_ros_extra::msg::PoseTarget Px4RosApi::transform_trajectory_from_map_to_odom(const px4_ros_extra::msg::PoseTarget &pose_target_msg_map)
{
    try
    {
        px4_ros_extra::msg::PoseTarget pose_target_msg_odom;

        // Lookup the "map" -> "odom" transform at the current time
        geometry_msgs::msg::TransformStamped map_to_odom_transform;
        map_to_odom_transform = _tf_buffer->lookupTransform(_odometry_link_frame, _map_link_frame, tf2::TimePointZero);

        // Create fake pose stamped message to apply transform
        geometry_msgs::msg::PoseStamped pose_stamped;
        geometry_msgs::msg::PoseStamped pose_stamped_transformed;

        pose_stamped.header.stamp = pose_target_msg_map.header.stamp;
        pose_stamped.header.frame_id = pose_target_msg_map.header.frame_id;

        pose_stamped.pose.position = pose_target_msg_map.position;
        pose_stamped.pose.orientation = pose_target_msg_map.orientation;

        tf2::doTransform(pose_stamped, pose_stamped_transformed, map_to_odom_transform);

        pose_target_msg_odom.position = pose_stamped_transformed.pose.position;
        pose_target_msg_odom.orientation = pose_stamped_transformed.pose.orientation;

        // Create fake vector3 stamped to apply transform
        geometry_msgs::msg::Vector3Stamped vector3_stamped;
        geometry_msgs::msg::Vector3Stamped vector3_stamped_transformed;

        vector3_stamped.header.stamp = pose_target_msg_map.header.stamp;
        vector3_stamped.header.frame_id = pose_target_msg_map.header.frame_id;

        vector3_stamped.vector = pose_target_msg_map.velocity;
        tf2::doTransform(vector3_stamped, vector3_stamped_transformed, map_to_odom_transform);
        pose_target_msg_odom.velocity = vector3_stamped_transformed.vector;

        vector3_stamped.vector = pose_target_msg_map.acceleration;
        tf2::doTransform(vector3_stamped, vector3_stamped_transformed, map_to_odom_transform);
        pose_target_msg_odom.acceleration = vector3_stamped_transformed.vector;

        vector3_stamped.vector = pose_target_msg_map.jerk;
        tf2::doTransform(vector3_stamped, vector3_stamped_transformed, map_to_odom_transform);
        pose_target_msg_odom.jerk = vector3_stamped_transformed.vector;

        vector3_stamped.vector = pose_target_msg_map.snap;
        tf2::doTransform(vector3_stamped, vector3_stamped_transformed, map_to_odom_transform);
        pose_target_msg_odom.snap = vector3_stamped_transformed.vector;

        // Do not transform quantities expressed in body frame
        pose_target_msg_odom.body_rate = pose_target_msg_map.body_rate;
        pose_target_msg_odom.body_rate_dot = pose_target_msg_map.body_rate_dot;

        return pose_target_msg_odom;
    }
    catch (tf2::TransformException &ex)
    {
        // Handle exceptions in case the transform is not available
        RCLCPP_INFO(this->get_logger(), "Could not transform %s to %s: %s", pose_target_msg_map.header.frame_id.c_str(), _odometry_link_frame.c_str(), ex.what());

        return pose_target_msg_map;
    }
}

int main(int argc, char *argv[])
{
    // Initialize the ROS 2 node
    rclcpp::init(argc, argv);

    // Create an instance of the Px4RosApi class
    auto px4_ros_api = std::make_shared<Px4RosApi>();

    // Spin the node to handle callbacks
    rclcpp::spin(px4_ros_api);

    // Shutdown ROS 2
    rclcpp::shutdown();
    return 0;
}
