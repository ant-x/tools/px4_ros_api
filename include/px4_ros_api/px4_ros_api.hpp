#ifndef PX4_ROS_API__PX4_ROS_API_HPP_
#define PX4_ROS_API__PX4_ROS_API_HPP_

#include <rclcpp/rclcpp.hpp>
#include <Eigen/Eigen>
#include <tf2/exceptions.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/buffer.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <sensor_msgs/msg/imu.hpp>                 // Standard ROS2 IMU message (https://docs.ros2.org/foxy/api/sensor_msgs/msg/Imu.html)
#include <nav_msgs/msg/odometry.hpp>               // Standard ROS2 odometry message (https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html)
#include <geometry_msgs/msg/pose_stamped.hpp>      // Standard ROS2 pose stamped message (https://docs.ros2.org/foxy/api/geometry_msgs/msg/PoseStamped.html)
#include <geometry_msgs/msg/transform_stamped.hpp> // Standard ROS2 transform stamped message (https://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/TransformStamped.html)
#include <geometry_msgs/msg/twist.hpp>             // Standard ROS2 twist message (https://docs.ros2.org/foxy/api/geometry_msgs/msg/Twist.html)

// Messages sent from FCU to be converted to ROS2 standard messages
#include "px4_msgs/msg/sensor_combined.hpp"  // PX4 sensor_combined message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/SensorCombined.msg)
#include "px4_msgs/msg/vehicle_odometry.hpp" // PX4 vehicle_odometry message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleOdometry.msg)

// Messages sent from ROS2 nodes to be converted to PX4 custom messages
#include "px4_msgs/msg/trajectory_setpoint.hpp"     // PX4 trajectory_setpoint message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/TrajectorySetpoint.msg)
#include "px4_msgs/msg/vehicle_visual_odometry.hpp" // PX4 vehicle_visual_odometry message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleVisualOdometry.msg)

// Additional PX4 messages
#include "px4_msgs/msg/vehicle_status.hpp"        // PX4 vehicle_status message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleStatus.msg)
#include "px4_msgs/msg/vehicle_command.hpp"       // PX4 vehicle_command message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleCommand.msg)
#include "px4_msgs/msg/offboard_control_mode.hpp" // PX4 offboard_control_mode message (https://github.com/PX4/px4_msgs/blob/release/1.13/msg/OffboardControlMode.msg)

// Custom messages
#include "px4_ros_extra/msg/uav_state.hpp"   // Custom uav_state message (https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/msg/UavState.msg)
#include "px4_ros_extra/msg/pose_target.hpp" // Custom pose_target message (https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/msg/PoseTarget.msg)

// Custom services
#include "px4_ros_extra/srv/arm_disarm.hpp" // Custom arm_disarm service (https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/ArmDisarm.srv)
#include "px4_ros_extra/srv/land.hpp"       // Custom land service (https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/Land.srv)
#include "px4_ros_extra/srv/takeoff.hpp"    // Custom takeoff service (https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/Takeoff.srv)

// Frame transformation library
#include "px4_ros_com/frame_transforms.h"

#define MAV_COMP_ID_AUTOPILOT1 1 // MAVLink Autopilot Component ID

using namespace px4_ros_com::frame_transforms;
using namespace px4_ros_com::frame_transforms::utils::quaternion;
using namespace px4_ros_com::frame_transforms::utils::types;
using Matrix6d = Eigen::Matrix<double, 6, 6>;

class Px4RosApi : public rclcpp::Node
{
public:
    Px4RosApi();

private:
    // Parameters
    int _system_id;                               // System ID parameter
    int _buffer_rate;                             // Buffer rate parameter
    std::string _base_link_frame;                 // Base link frame parameter
    std::string _odometry_link_frame;             // Odometry link frame parameter
    bool _publish_map_odom_tf_flag;               // Publish map -> odom TF
    std::string _map_link_frame;                  // Map link frame parameter
    std::vector<double> _map_to_odom_translation; // Translation between "map" and "odom" frames
    std::vector<double> _map_to_odom_rpy;         // Roll, pitch, and yaw angles for the transformation between "map" and "odom" frames
    bool _subscribe_to_external_odometry;         // Subscribe to external odometry (i.e., visual odometry)
    bool _subscribe_to_external_pose;             // Subscribe to external pose (i.e., MoCap pose)
    int _maximum_ext_rate;                        // Maximum rate parameter for external odometry or external pose

    // State
    enum class UavState
    {
        DISARMED, // Disarmed state
        ARMED,    // Armed state
        TAKEOFF,  // Takeoff state
        HOLD,     // Hold state
        INFLIGHT, // Inflight state
        PILOTED,  // Piloted state
        LANDING   // Landing state
    };
    UavState _uav_state = UavState::DISARMED; // UAV state
    bool _fcu_armed{false};
    bool _fcu_offboard{false};
    bool _arm_request{false};
    bool _disarm_request{false};
    bool _land_request{false};
    bool _takeoff_request{false};
    bool _received_velocity_sp{false};
    bool _received_trajectory_sp{false};
    void updateStateMachine();

    // Variables
    rclcpp::TimerBase::SharedPtr _timer; // Timer
    rclcpp::TimerBase::SharedPtr _timer_visual_odometry; //  Timer for visual odometry timer callback
    float _odom_offset_north = 0.0f;
    float _odom_offset_east = 0.0f;
    float _odom_offset_down = 0.0f;
    uint64_t _old_vehicle_visual_odometry_msg_timestamp{};

    // QoS
    rclcpp::QoS _qos_default = rclcpp::QoS(rclcpp::KeepLast(1));
    rclcpp::QoS _qos_latched = rclcpp::QoS(rclcpp::KeepLast(1));

    // Subscribers and publishers
    rclcpp::Subscription<px4_msgs::msg::VehicleStatus>::SharedPtr _vehicle_status_sub;        // Vehicle status subscriber
    rclcpp::Subscription<px4_msgs::msg::SensorCombined>::SharedPtr _sensor_combined_sub;      // Sensor combined subscriber
    rclcpp::Subscription<px4_msgs::msg::VehicleOdometry>::SharedPtr _vehicle_odometry_sub;    // Vehicle odometry subscriber
    rclcpp::Subscription<px4_ros_extra::msg::PoseTarget>::SharedPtr _trajectory_setpoint_sub; // Trajectory setpoint subscriber
    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr _external_odometry_sub;          // External odometry subscriber
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr _external_pose_sub;      // External pose subscriber
    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr _velocity_setpoint_sub;        // Velocity setpoint subscriber

    rclcpp::Publisher<sensor_msgs::msg::Imu>::SharedPtr _std_imu_pub;                                // Standard IMU publisher
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr _std_odometry_pub;                         // Standard odometry publisher
    rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr _std_pose_stamped_pub;             // Standard pose stamped publisher
    rclcpp::Publisher<px4_ros_extra::msg::UavState>::SharedPtr _uav_state_pub;                       // UAV state publisher
    rclcpp::Publisher<px4_msgs::msg::VehicleCommand>::SharedPtr _vehicle_command_pub;                // Vehicle command publisher
    rclcpp::Publisher<px4_msgs::msg::TrajectorySetpoint>::SharedPtr _trajectory_setpoint_pub;        // Trajectory setpoint publisher
    rclcpp::Publisher<px4_msgs::msg::OffboardControlMode>::SharedPtr _offboard_control_mode_pub;     // Offboard control mode publisher
    rclcpp::Publisher<px4_msgs::msg::VehicleVisualOdometry>::SharedPtr _vehicle_visual_odometry_pub; // Vehicle visual odometry publisher

    // Services
    rclcpp::Service<px4_ros_extra::srv::ArmDisarm>::SharedPtr _arm_disarm_server; // Arm/disarm service server
    rclcpp::Service<px4_ros_extra::srv::Land>::SharedPtr _land_server;            // Land service server
    rclcpp::Service<px4_ros_extra::srv::Takeoff>::SharedPtr _takeoff_server;      // Takeoff service server

    // Messages
    px4_msgs::msg::VehicleStatus _vehicle_status_msg;              // Vehicle status message
    px4_msgs::msg::SensorCombined _sensor_combined_msg;            // Sensor combined message
    px4_msgs::msg::VehicleOdometry _vehicle_odometry_msg;          // Vehicle odometry message
    px4_ros_extra::msg::PoseTarget _trajectory_setpoint_input_msg; // Trajectory setpoint input message
    nav_msgs::msg::Odometry _external_odometry_msg;                // External odometry message
    geometry_msgs::msg::PoseStamped _external_pose_msg;            // External pose message
    geometry_msgs::msg::Twist _velocity_setpoint_input_msg;        // Velocity setpoint input message

    sensor_msgs::msg::Imu _output_std_imu_msg;                         // Standard IMU message
    nav_msgs::msg::Odometry _output_std_odom_msg;                      // Standard odometry message
    geometry_msgs::msg::PoseStamped _output_std_pose_stamped;          // Standard pose message
    px4_ros_extra::msg::UavState _uav_state_msg;                       // UAV state message
    px4_msgs::msg::VehicleCommand _vehicle_command_msg;                // Vehicle command message
    px4_msgs::msg::TrajectorySetpoint _trajectory_setpoint_aux_msg;    // Trajectory setpoint aux message
    px4_msgs::msg::TrajectorySetpoint _trajectory_setpoint_output_msg; // Trajectory setpoint output message
    px4_msgs::msg::OffboardControlMode _offboard_control_mode_msg;     // Offboard control mode message
    px4_msgs::msg::VehicleVisualOdometry _vehicle_visual_odometry_msg; // Vehicle visual odometry message

    // TF broadcasters
    std::shared_ptr<tf2_ros::StaticTransformBroadcaster> _map_to_odom_tf_broadcaster;
    std::shared_ptr<tf2_ros::TransformBroadcaster> _odom_to_base_link_tf_broadcaster;

    // TF listener
    std::unique_ptr<tf2_ros::Buffer> _tf_buffer;
    std::shared_ptr<tf2_ros::TransformListener> _tf_listener{nullptr};

    // Callbacks
    void vehicleStatusCallback(const px4_msgs::msg::VehicleStatus::SharedPtr msg);        // Vehicle status callback
    void sensorCombinedCallback(const px4_msgs::msg::SensorCombined::SharedPtr msg);      // Sensor combined callback
    void vehicleOdometryCallback(const px4_msgs::msg::VehicleOdometry::SharedPtr msg);    // Vehicle odometry callback
    void trajectorySetpointCallback(const px4_ros_extra::msg::PoseTarget::SharedPtr msg); // Trajectory setpoint callback
    void externalOdometryCallback(const nav_msgs::msg::Odometry::SharedPtr msg);          // External odometry callback
    void externalPoseCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg);      // External Pose callback
    void velocitySetpointCallback(const geometry_msgs::msg::Twist::SharedPtr msg);        // Velocity setpoint callback
    void mainLoop();                                                                      // Trajectory setpoint timer callback
    void visualOdometryPublishTimerCallback();                                            // Visual odometry publish timer callback

    // PX4 features
    void sendVehicleCommand(uint16_t command, float param1 = 0.0, float param2 = 0.0); // Send vehicle command

    // Service handling
    void handleArmDisarm(
        const std::shared_ptr<px4_ros_extra::srv::ArmDisarm::Request> request,
        const std::shared_ptr<px4_ros_extra::srv::ArmDisarm::Response> response); // Arm/disarm service handler
    void handleLand(
        const std::shared_ptr<px4_ros_extra::srv::Land::Request> request,
        const std::shared_ptr<px4_ros_extra::srv::Land::Response> response); // Land service handler
    void handleTakeoff(
        const std::shared_ptr<px4_ros_extra::srv::Takeoff::Request> request,
        const std::shared_ptr<px4_ros_extra::srv::Takeoff::Response> response); // Takeoff service handler

    // Publish tf
    void publish_map_to_odom_tf();                                         // Publish static tf between map and odom
    void publish_odom_to_base_link_tf(const nav_msgs::msg::Odometry &msg); // Publish dynamic tf between odom and base_link

    // Auxiliary function
    void array_urt_to_covariance_matrix_custom(const std::array<float, 21> &covmsg, Matrix6d &covmat);                   // Array to covariance matrix conversion
    void covariance_matrix_to_array_urt_custom(const Matrix6d &covmat, std::array<float, 21> &covmsg);                   // Covariance matrix to array conversion
    geometry_msgs::msg::PoseStamped transform_odometry_to_map_pose(const nav_msgs::msg::Odometry &odometry);             // Transform odometry msg to pose msg refferred to map
    px4_ros_extra::msg::PoseTarget transform_trajectory_from_map_to_odom(const px4_ros_extra::msg::PoseTarget &msg_map); // transform trajectory msg from map to odom frame
};

#endif // PX4_ROS_API__PX4_ROS_API_HPP_
