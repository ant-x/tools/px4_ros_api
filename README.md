# PX4 ROS API

The PX4 ROS API project is a ROS2 node which interfaces the topics coming from the MicroRTPS Bridge to the ROS2 network and vice-versa.
It also provide services to control the drone as arm/disarm, takeoff/landing and trajectory control.

## Reference frames

As described in the [ROS REP 105](https://www.ros.org/reps/rep-0105.html) the involved reference frames are the following:

* `map frame`: a world-fixed frame (East-North-Up, *ENU*) common among the drones.
* `odom frame`: a world-fixed frame (East-North-Up, *ENU*) located at the starting point of the drone.
* `base_link frame`: a body-fixed frame (Front-Left-Up, *FLU*).

### NED/ENU Conversion

Data coming from PX4 assumes a world-fixed frame expressed ad a North-East-Down (*NED*) frame and a body-fixed frame expressed as a Front-Right-Down (*FRD*) frame.
However, to ensure consistency with ROS standard conventions, it is necessary to convert this data into the East-North-Up (*ENU*) and Front-Left-Up (*FLU*) coordinate systems.
Moreover, the setpoints that must be sent to the PX4 flight controller which are sent from other ROS nodes must be converted from *ENU* to *NED* and from *FLU* to *FRD* reference frames.
These conversion are managed by the PX4 ROS API node.

## Parameters

* `system_id`: Mavlink System ID (default: `1`)

* `buffer_rate`: trajectory send frequency to PX4 (default: `50`)

* `base_link_frame`: body attached frame name (default: `"base_link_1"`)

* `odometry_link_frame`: odometry frame name(default: `"odom_1"`)

* `publish_map_odom_tf`: if set to True, publish a static TF between map and odom frames (default: `True`)

* `map_link_frame`: map frame name (default: `"map"`)

* `map_to_odom_translation`: position of the odom frame with respect to the map frame (default: `[0.0, 0.0, 0.0]`)

* `map_to_odom_rpy`: orientation (expressed as Euler Roll, Pitch and Yaw angles) of the odom frame with respect to the map frame (default: `[0.0, 0.0, 0.0]`)

## Input topics

From PX4:

* `fmu/timesync/out` ([px4_msgs/msg/Timesync](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/Timesync.msg)) - Topic needed to synchronize time when publishing messages to PX4.
* `fmu/vehicle_status/out` ([px4_msgs/msg/VehicleStatus](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleStatus.msg)) - Topic containing information about the vehicle status (*e.g.*, armed/disarmed, ...).
* `fmu/sensor_combined/out` ([px4_msgs/msg/SensorCombined](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/SensorCombined.msg)) - Topic containing gyroscope and accelerometer information expressed in *body_frame* (*FRD*).
* `fmu/vehicle_odometry/out` ([px4_msgs/msg/VehicleOdometry](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleOdometry.msg)) - Topic containing vehicle odometry of the drone *base_link* frame (*FRD*) expressed in *odom* frame (*NED*).

From ROS2:

* `px4_ros_api/trajectory/in` ([px4_ros_extra/msg/PoseTarget](https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/msg/PoseTarget.msg)) - Topic containing the full position setpoint (position, velocity, acceleration, snap, jerk) of the drone *base_link* frame (*FLU*) expressed in *map* frame (*ENU*), the quaternion target (*x,y,z,w*) representing the desired orientation of the drone *base_link* frame (*FLU*) with respect to the *map* frame (*ENU*) and the target angular velocity/acceleration of the *base_link* frame (*FLU*).

## Output topics

To PX4:

* `fmu/offboard_control_mode/in` ([px4_msgs/msg/OffboardControlMode](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/OffboardControlMode.msg)) - Topic needed to send the offboard control mode.
* `fmu/trajectory_setpoint/in` ([px4_msgs/msg/TrajectorySetpoint](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/TrajectorySetpoint.msg)) - Topic including the attitude and position target setpoints expressed in *odom* frame (*NED*).
* `fmu/vehicle_command/in` ([px4_msgs/msg/VehicleCommand](https://github.com/PX4/px4_msgs/blob/release/1.13/msg/VehicleCommand.msg)) - Topic needed to send commands as arm/disarm.

To ROS2:

* `px4_ros_api/imu/out` ([sensor_msgs/msg/Imu](https://docs.ros2.org/foxy/api/sensor_msgs/msg/Imu.html))- Topic containing gyroscope and accelerometer information expressed in *body_frame* (*FLU*) and a quaternion (*x,y,z,w*) representing the orientation of the drone *base_link* frame (*FLU*) with respect to the *odom* frame (*ENU*).
* `px4_ros_api/odometry/out` ([nav_msgs/msg/Odometry](https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html)) - Topic containing vehicle odometry of the drone *base_link* frame (*FLU*) expressed in *odom* frame (*ENU*).
* `px4_ros_api/pose_stamped/out` ([geometry_msgs/msg/PoseStamped](https://docs.ros2.org/foxy/api/geometry_msgs/msg/PoseStamped.html)) - Topic containing vehicle pose of the drone *base_link* frame (*FLU*) expressed in *map* frame (*ENU*).
* `px4_ros_api/state/out` ([px4_ros_extra/msg/UavState](https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/msg/UavState.msg)) - Topic containing the UAV state machine state.

## Services Provided

* `px4_ros_api/srv/arm_disarm` ([px4_ros_extra/srv/ArmDisarm](https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/ArmDisarm.srv)) - Service provided to arm/disarm the drone.
* `px4_ros_api/srv/land` ([px4_ros_extra/srv/Land](https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/Land.srv)) - Service provided to land the drone.
* `px4_ros_api/srv/takeoff` ([px4_ros_extra/srv/Takeoff](https://gitlab.com/ant-x/tools/px4_ros_extra/-/blob/master/srv/Takeoff.srv)) - Service provided to takeoff the drone.

## State Machine

The behaviour of this node is controlled by a state machine as descibed in the picture below:

<div align="center">
  <img src="figures/px4_ros_api_state_machine.drawio.svg" width="600">
</div>

## Publishing Trajectory Data

When the UAV is in the "INFLIGHT" state, the PX4 ROS API publishes trajectory data at a specified frequency. 
This trajectory data contains information for guiding the UAV's movement, such as position, orientation, velocity, and acceleration setpoints.

The trajectory data is published at a frequency determined by the parameter `buffer_rate`, which specifies the desired rate of data transmission.

## Code overview

The `px4_ros_api.hpp` file contains the implementation of a ROS2 node named `Px4RosApi`. 
This node serves as an interface between PX4 autopilot messages and ROS2 standard messages. 
It subscribes to various topics published by the PX4 autopilot, processes the received messages, and publishes corresponding ROS2 standard messages. 
Additionally, it provides services for arming/disarming and takeoff/landing commands.

Class `Px4RosApi`
* `Constructors`
    * `Px4RosApi()`: 
        Constructs a `Px4RosApi` node. 
        It initializes parameters, declares subscribers, publishers, services, and creates a timer to trigger periodic tasks.

* Private Member Variables
    * `_system_id`: Stores the system ID parameter.
    * `_buffer_rate`: Stores the buffer rate parameter.
    * `_odometry_link_frame`: Stores the odometry link frame parameter.
    * `_base_link_frame`: Stores the base link frame parameter.
    * `_publish_map_odom_tf_flag`: Flag to publish the transformation between "map" and "odom" frames.
    * `_map_link_frame`: Stores the map link frame parameter.
    * `_map_to_odom_translation`: Translation between "map" and "odom" frames.
    * `_map_to_odom_rpy`: Roll, pitch, and yaw angles for the transformation between "map" and "odom" frames.
    * `_uav_state`: Represents the state of UAV (Armed/Disarmed).
    * `_timestamp`: Atomic variable to store a synchronized timestamp.
    * `_timer`: Timer for periodic tasks.
    * `_takeoff_altitude`: Takeoff altitude.
    * `_trajectory_received`: Flag indicating if a trajectory setpoint has been received.
    * `_qos_default`: Default quality of service for messages.
    * `_qos_latched`: Latched quality of service for messages.
    * Subscribers and publishers for various message types.
    * Services for arming/disarming and takeoff/landing.
    * Variables to store received messages.

* Private Member Functions
    * `vehicleStatusCallback()`: Callback for handling VehicleStatus messages.
    * `sensorCombinedCallback()`: Callback for handling SensorCombined messages.
    * `vehicleOdometryCallback()`: Callback for handling VehicleOdometry messages.
    * `trajectorySetpointCallback()`: Callback for handling PoseTarget messages.
    * `trajectorySetpointTimerCallback()`: Callback for periodically publishing trajectory setpoints.
    * `handleArmDisarm()`: Service handler for arming/disarming requests.
    * `handleTakeoffLand()`: Service handler for takeoff/landing requests.
    * `sendVehicleCommand()`: Sends vehicle command messages to the PX4 autopilot.
    * `array_urt_to_covariance_matrix_custom()`: Converts covariance message array to covariance matrix.

* Main Function
    * `main()`: Initializes the ROS2 node and spins it.

Additional Files:
* `px4_ros_api.hpp`: Header file containing the class declaration for `Px4RosApi`.
* `launch.py`: Launch file for starting the `px4_ros_api` node with specified parameters and remappings.
* `params.yaml`: YAML file containing node parameters.

Launch File (`launch.py`)
* The launch file starts the `px4_ros_api` node with specified parameters and remappings.

Parameter File (`params.yaml`)
* Contains parameters for the px4_ros_api node, such as system_id, buffer_rate, odometry_link_frame, and base_link_frame.

## Contributions

Contributions are welcome! For suggestions, improvements, or bug reports, refer to the "Issues" section on GitLab.

## License

The PX4 ROS API is released under the [BSD-3-Clause License](LICENSE).

## Authors

- Mattia Giurato [mattia@antx.it](mailto:mattia@antx.it)
